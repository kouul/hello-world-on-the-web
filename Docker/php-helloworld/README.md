# Hello World

### Info
Source code for a PHP Hello World using Docker containers.

### Installation guide:
- docker-compose up -d

### Images used:
- php:7.0-apache

Open [http://localhost:5566](http://localhost:5566) to view it in the browser.