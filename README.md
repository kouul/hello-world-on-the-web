# Hello World

### Aim
This project aims at trying to display "Hello World" on your browser using all possible web frameworks such as ReactJS & Flask.

### Info
The repository is structured into folders named using respective programming language. For example, Python folder will contain sub folders named Flask or Django. Each of these sub folder will contain the source code for the Hello World project along with a Readme file which will elaborate on the installation guide.

### How to contribute
1. Fork the repository
2. Clone the forked repo to your local environment
   - $ git clone https://github.com/{{ your-username }}/hello-world-on-the-web
3. Make your changes
4. Create a branch and checkout
   - $ git branch branch_name
   - $ git checkout branch_name
5. Create pull rqeuest

### Languages covered
##### HTML

##### Python
- [x] Flask
- [x] Django
- [ ] web2py
- [ ] cherrypy

##### Java
- [ ] Play
- [ ] Spring
- [ ] Struts

##### Javascript
- [ ] NodeJs
- [x] ReactJs
- [x] ExtJs
- [ ] AngularJs
- [ ] EmberJs

##### Docker
- [x] Flask
- [x] PHP
- [ ] ReactJs
- [ ] AngularJs
- [ ] Django

##### Other

Feel free to commit :)